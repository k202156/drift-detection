#!/bin/bash

# Remove existing containers with the specified names
docker rm -f mongodb || true
docker rm -f flask_container || true
docker rm -f streamlit_container || true
docker rm -f drift_detection_container || true
