
import os
import sys
# Get the parent directory of the current script
current_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))

# Add the parent directory to the Python path
sys.path.append(parent_dir)

import os
import sys
import copy
import datetime
import string
import secrets
from functools import partial
from typing import Optional, Union

import numpy as np
import matplotlib.pyplot as plt
from scipy.spatial.distance import pdist
from scipy.stats import gaussian_kde
import torch
from torch import nn
from torch.utils.data import Dataset
import torchvision
import requests
from flask import request, jsonify

from configuration.settings import Config
from frouros.callbacks import PermutationTestDistanceBased
from frouros.detectors.data_drift import MMD
from frouros.utils.kernels import rbf_kernel
import json
import pickle
from frouros.detectors.concept_drift import DDM, DDMConfig
from sklearn.metrics import mean_squared_error


# Set random seed
def set_seed(seed=31):
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
    torch.manual_seed(seed)
    np.random.seed(seed)

set_seed(seed=Config.seed)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")


def generate_random_string(length=12):
    characters = string.ascii_letters + string.digits
    return ''.join(secrets.choice(characters) for _ in range(length))


def send_data_to_zenodo():
    with open(Config.file_path, 'rb') as file:
        response = requests.put(Config.upload_url_zenodo, data=file, headers=Config.headers_zenodo)
    print("File uploaded:", response.text)


def send_slack_notification():
    payload = {
        "channel": "#cygnss",
        "username": "webhookbot",
        "text": "Drift detected in the drift-detection pipeline, data could not be uploaded to Zenodo",
        "icon_emoji": ":ghost:"
    }
    response = requests.post(Config.webhook_url_slack, json=payload)
    print(response.text)


def update_status_of_job(status, drift_id):
    params = {
        'drift_run_id': drift_id,
        'new_status': status
    }
    url = Config.monitoring_url + '/update_status'
    response = requests.put(url, params=params, auth=(Config.api_username, Config.api_password))
    return response.status_code


def save_drift_run(status):
    try:
        params = {        
            'job_status': status,
            'datetime' : datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")
        }
        url = Config.monitoring_url + '/save_drift_run'
        response = requests.post(url, json=params, auth=(Config.api_username, Config.api_password))

        print('saved drift run', response.text)
        
        return response

    except requests.exceptions.RequestException as e:
        # Handle request exceptions (e.g., network errors)
        return {"error": f"Request failed: {str(e)}"}



def train_autoencoder():
    transform = torchvision.transforms.Compose([
        torchvision.transforms.ToTensor(),
    ])

    train_all_dataset = torchvision.datasets.MNIST(
        root="/tmp/mnist/train/",
        train=True,
        download=True,
        transform=transform,
    )

    train_all_dataset_size = len(train_all_dataset)
    train_dataset_size = int(train_all_dataset_size * 0.5)
    reference_dataset_size = train_all_dataset_size - train_dataset_size

    train_dataset, reference_dataset = torch.utils.data.random_split(
        dataset=train_all_dataset,
        lengths=[train_dataset_size, reference_dataset_size],
        generator=torch.Generator().manual_seed(31)
    )

    train_data_loader = torch.utils.data.DataLoader(
        dataset=train_dataset,
        batch_size=Config.batch_size,
        shuffle=True
    )

    autoencoder = Autoencoder(latent_dim=Config.latent_dim)

    optimizer = torch.optim.Adam(autoencoder.parameters(), lr=Config.learning_rate)

    for epoch in range(Config.epochs):
        running_loss = 0.0
        for i, (inputs, _) in enumerate(train_data_loader, 0):
            inputs.requires_grad = True
            inputs.retain_grad()

            outputs_e, outputs = autoencoder(inputs)
            loss = autoencoder.calculate_loss(outputs_e, outputs, inputs)

            inputs.requires_grad = False

            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

            running_loss += loss.item()

            if i % 100 == 99:
                print(f'[{epoch + 1}, {i + 1:5d}] loss: {running_loss / 100:.3f}')
                running_loss = 0.0

    return autoencoder,reference_dataset


class Autoencoder(nn.Module):
    def __init__(self, latent_dim: int) -> None:
        super().__init__()
        self.encoder = Encoder(latent_dim=latent_dim)
        self.decoder = Decoder(latent_dim=latent_dim)

    def forward(self, x):
        encoded = self.encoder(x)
        decoded = self.decoder(encoded)
        return encoded, decoded

    def calculate_loss(self, outputs_e, outputs, inputs, lamda=1e-4):
        criterion = nn.MSELoss()
        loss1 = criterion(outputs, inputs)

        outputs_e.backward(torch.ones(outputs_e.size()), retain_graph=True)
        loss2 = torch.sqrt(torch.sum(torch.pow(inputs.grad, 2)))
        inputs.grad.data.zero_()

        loss = loss1 + (lamda * loss2)
        return loss


class Encoder(nn.Module):
    def __init__(self, latent_dim: int):
        super().__init__()
        self.encoder_conv = nn.Sequential(
            nn.Conv2d(1, 8, 3, stride=2, padding=1),
            nn.ReLU(True),
            nn.Conv2d(8, 16, 3, stride=2, padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU(True),
            nn.Conv2d(16, 32, 3, stride=2, padding=0),
            nn.ReLU(True)
        )

        self.flatten = nn.Flatten(start_dim=1)

        self.encoder_lin = nn.Sequential(
            nn.Linear(3 * 3 * 32, 128),
            nn.ReLU(True),
            nn.Linear(128, latent_dim)
        )

    def forward(self, x):
        x = self.encoder_conv(x)
        x = self.flatten(x)
        x = self.encoder_lin(x)
        return x


class Decoder(nn.Module):
    def __init__(self, latent_dim: int):
        super().__init__()
        self.decoder_lin = nn.Sequential(
            nn.Linear(latent_dim, 128),
            nn.ReLU(True),
            nn.Linear(128, 3 * 3 * 32),
            nn.ReLU(True)
        )

        self.unflatten = nn.Unflatten(dim=1, unflattened_size=(32, 3, 3))

        self.decoder_conv = nn.Sequential(
            nn.ConvTranspose2d(32, 16, 3, stride=2, output_padding=0),
            nn.BatchNorm2d(16),
            nn.ReLU(True),
            nn.ConvTranspose2d(16, 8, 3, stride=2, padding=1, output_padding=1),
            nn.BatchNorm2d(8),
            nn.ReLU(True),
            nn.ConvTranspose2d(8, 1, 3, stride=2, padding=1, output_padding=1)
        )

    def forward(self, x):
        x = self.decoder_lin(x)
        x = self.unflatten(x)
        x = self.decoder_conv(x)
        x = torch.sigmoid(x)
        return x


class CustomMNIST(Dataset):
    def __init__(self, subset, transform: Optional[Union[torch.nn.Module, torchvision.transforms.Compose]] = None):
        self.subset = subset
        self.transform = transform

    def __getitem__(self, index):
        x, y = self.subset[index]
        if self.transform:
            x = self.transform(x)
        return x, y

    def __len__(self) -> int:
        return len(self.subset)


def main():
    try:           
        detector = None
        drift_run_id = None
        # Make an entry as the job starts
        response = save_drift_run('started')

        response_data = response.json()

        if response.status_code == 200:
            drift_run_id = response_data['drift_run_id']
            
        autoencoder, reference_dataset = train_autoencoder()

        test_dataset = torchvision.datasets.MNIST(
            root="/tmp/mnist/test/",
            train=False,
            download=True,
        )

        gaussian_blur_dataset = CustomMNIST(
            subset=test_dataset,
            transform=torchvision.transforms.Compose([
                torchvision.transforms.GaussianBlur(kernel_size=(5, 9), sigma=1.25),
                torchvision.transforms.ToTensor(),
            ]),
        )

        elastic_transform_dataset = CustomMNIST(
            subset=test_dataset,
            transform=torchvision.transforms.Compose([
                torchvision.transforms.ElasticTransform(alpha=60.0, sigma=5.0),
                torchvision.transforms.ToTensor(),
            ]),
        )

        idx_ref_sample = np.random.choice(np.arange(0, len(reference_dataset)), size=Config.num_samples, replace=False)
        X_ref_sample = np.array([X_sample.tolist() for X_sample, _ in torch.utils.data.Subset(reference_dataset, idx_ref_sample)]).astype(np.float32)

        idx_test_sample = np.random.choice(np.arange(0, len(test_dataset)), size=Config.num_samples, replace=False)
        X_test_original_sample = np.array([torchvision.transforms.ToTensor()(X_sample).tolist() for X_sample, _ in torch.utils.data.Subset(test_dataset, idx_test_sample)]).astype(np.float32)
        X_test_gaussian_blur_sample = np.array([X_sample.tolist() for X_sample, _ in torch.utils.data.Subset(gaussian_blur_dataset, idx_test_sample)]).astype(np.float32)
        X_test_elastic_transform_sample = np.array([X_sample.tolist() for X_sample, _ in torch.utils.data.Subset(elastic_transform_dataset, idx_test_sample)]).astype(np.float32)

        with torch.no_grad():
            X_ref_encoded = autoencoder.encoder(torch.Tensor(X_ref_sample)).numpy()

            X_test_original_encoded = autoencoder.encoder(torch.Tensor(X_test_original_sample)).numpy()
            X_test_gaussian_blur_encoded = autoencoder.encoder(torch.Tensor(X_test_gaussian_blur_sample)).numpy()
            X_test_elastic_transform_encoded = autoencoder.encoder(torch.Tensor(X_test_elastic_transform_sample)).numpy()

        sigma = np.median(pdist(X=X_ref_encoded, metric="euclidean"))

        # Define the parameters for the request
        params = {
        "start_date": "1999-01-01T00:00:00",  # Example start date
        "end_date": "2030-01-31T23:59:59",    # Example end date
        "drift_detection": None    # Example drift detection type
        }


        response  = requests.get(Config.monitoring_url + '/get_data_with_multiple_filters', auth=(Config.api_username, Config.api_password), params=params)

        if response.status_code == 200:
            pickled_data = response.content

            deserialized_detector = pickle.loads(pickled_data)
            detector = deserialized_detector
        
        
        # if there is a problem in getting a detector
        elif response.status_code == 204:
        
            detector = MMD(
                kernel=partial(rbf_kernel, sigma=sigma),
                callbacks=[
                    PermutationTestDistanceBased(
                        num_permutations=Config.num_permutations,
                        random_state=Config.seed,
                        num_jobs=-1,
                        name="permutation_test",
                        verbose=False,
                    ),
                ],
            )

            # # make a api request and get the last row where detector is not None
            # # use that detector. If it is None then train the detector and save it, irrespective of the drift result.
            
            detector.fit(X=X_ref_encoded)


        permutation_test_logs = {}

        for sample, type_ in zip([X_test_original_encoded, X_test_gaussian_blur_encoded, X_test_elastic_transform_encoded],
                                ["Original (unmodified)", "Gaussian Blur", "Elastic Transform"]):
            mmd, callbacks_logs = detector.compare(X=sample)
            permutation_test_logs[type_] = copy.copy(callbacks_logs["permutation_test"])
            mmd, p_value = mmd.distance, callbacks_logs["permutation_test"]["p_value"]
            drift = p_value <= Config.alpha
            print(f"{type_}:\n  MMD statistic={round(mmd, 4)}, p-value={round(p_value, 4)}, drift={drift}")


        num_bins = 50
        x_values = 100
        num_percentile = 100 - Config.alpha * 100

        n_rows = 3
        fig, axs = plt.subplots(nrows=n_rows, ncols=1, figsize=(8, 8), sharex=True, sharey=True, dpi=600)
        y_lim = 0.0

        for ax, (type_, permutation_test) in zip(axs.flat, permutation_test_logs.items()):
            permutation_tests = permutation_test["permuted_statistics"]
            observed_statistic = permutation_test["observed_statistic"]
            p_value = permutation_test["p_value"]

            ax.hist(permutation_tests, bins=num_bins, density=True)
            xs = np.linspace(min(permutation_tests), max(permutation_tests), num=x_values)
            permutation_tests_density = gaussian_kde(permutation_tests).evaluate(xs)
            ax.plot(xs, permutation_tests_density, label="KDE")
            ax.axvline(observed_statistic, color="green", linestyle="--", label="Observed distance")
            ax.set_title(type_)
            drift = p_value <= Config.alpha
            ax.text(0.825, 0.15, "Drift" if drift else "No drift", transform=ax.transAxes, fontsize=12, bbox={"boxstyle": "round", "facecolor": "red" if drift else "green", "alpha": 0.5})
            ax.text(0.8, 0.05, f"p-value={round(p_value, 4)}", transform=ax.transAxes, fontsize=8)

            ax_y_lim = max(ax.get_lines()[0].get_ydata())
            if y_lim < ax_y_lim:
                y_lim = ax_y_lim

        for ax, (_, permutation_test) in zip(axs.flat, permutation_test_logs.items()):
            permutation_tests = permutation_test["permuted_statistics"]
            p_value = permutation_test["p_value"]

            percentile = np.percentile(permutation_tests, q=num_percentile)
            ax.axvline(percentile, color="red", linestyle="--", label="Significance threshold")
            ax.legend(fontsize=8, loc="upper right")

        fig.supxlabel("MMD²")
        fig.supylabel("Density")
        fig.tight_layout()
        plt.savefig("./drift_detection.png", dpi=fig.dpi)

        data_drift = any(p_value <= Config.alpha for p_value in [permutation_test_logs[type_]["p_value"] for type_ in permutation_test_logs])
        formatted_datetime = datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

        filehandler = open('./detector.pkl', 'wb') 
        pickle.dump(detector, filehandler)

        pickled_serialized_detector = open('./detector.pkl', 'rb')

        # Concept Drift Detector

        config = DDMConfig(warning_level=2.0,
                    drift_level=3.0,
                    min_num_instances=1000,)
        
        concept_drift_detector = DDM(config=config)

        ground_truth  = autoencoder.encoder(torch.Tensor(X_ref_sample)).detach().numpy()
        
        concept_drift_message = None

        for i, (X, y) in enumerate(zip(X_test_gaussian_blur_sample, ground_truth)):
            X = torch.unsqueeze(torch.Tensor(X), 0)
            prediction = autoencoder.encoder(torch.Tensor(X))
            prediction = torch.squeeze(prediction, 0).detach().numpy()
            error = mean_squared_error(prediction, y)
            concept_drift_detector.update(value=error)
            status = concept_drift_detector.status            
            if status["drift"]:
                print(f"Drift detected at index {i}")
                concept_drift_message = f"Drift detected at index: {i}"
                break
            else:
                concept_drift_message = "No concept drift detected"

        concept_drift = status["drift"]

        drift_run_data = {        
            "drift_run_id": drift_run_id,
            "datetime": formatted_datetime,
            "job_status": "complete",
            "data_version": "v1",            
            "data_drift": {
                "drift": data_drift,
                "drift_parameter": [{"p_value": p_value} for p_value in [permutation_test_logs[type_]["p_value"] for type_ in permutation_test_logs]],
            },
            "concept_drift": {
                "drift": concept_drift,
                "drift_parameter": [{"message" : concept_drift_message}]
            }
        }

        
        url = Config.monitoring_url + '/save_drift_run'
        
        if not concept_drift and not data_drift:
            files = [
            ('document', ('local_file_to_send', pickled_serialized_detector, 'application/octet')),
            ('datas', ('datas', json.dumps(drift_run_data), 'application/json')),
        ]
    
        else:
        
            files = [            
                ('datas', ('datas', json.dumps(drift_run_data), 'application/json')),
            ]

        
        response = requests.put(url, files=files, auth=(Config.api_username, Config.api_password))


        if response.status_code == 200 or response.status_code == 201:            
            if data_drift or concept_drift:
                send_slack_notification()
            else:
                send_data_to_zenodo()
        
            print("job completed")

        else:
            print("Failed to save drift run data. Status code:", response.status_code)

        

    except Exception as e:        
        
        print("job failed", e)

        drift_run_data = {        
            "drift_run_id": drift_run_id,
            "datetime": formatted_datetime,
            "job_status": "Failed",            
        }

        url = Config.monitoring_url + '/save_drift_run'
        
        response = requests.put(url, json=drift_run_data, auth=(Config.api_username, Config.api_password))


if __name__ == "__main__":
    main()
