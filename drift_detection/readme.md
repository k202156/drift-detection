# Drift Detection

## Overview

For now, this repository contains two main components:

1. **Flask API and Mongo DB(`backend.py`):**
   - A Flask API that handles drift run data and job status.
   - Saves drift run data to a MongoDB database.
   - Provides endpoints for saving job status, getting data with multiple filters, and triggering a GitLab pipeline.

2. **Streamlit Dashboard (`drift_run_dashboard.py`):**
   - A Streamlit dashboard for visualizing feature drift, concept drift, and no drift.
   - Fetches data from the Flask API using various endpoints.

3. **Swagger documentation (`Swagger_API_doc.yml`):**
   - Contains swagger documentation (openapi 3.0.0) for above APIs.

## Setup Conda Environment:

  Create a conda environment, make sure conda is installed (https://conda.io/docs/user-guide/install/):
    
     - conda create --name drift-run python=3.10
     - conda activate drift-run
     - pip install -r requirements.txt


### Execute:

#### Run API server:

        - python backend.py
        - Access APIs documentation here: http://127.0.0.1:8000/api/ui/

#### Run Streamlit Server:

    - streamlit run drift_run_dashboard.py
    - Access Dashboard here: http://localhost:8501/


