# Code Review Results

## `/backend/backend.py`
### Position: `12:0-13:0`
* Priority: `0`
* Title: `Use token bearer`
* Category: `Other`
* Description: `auth &#x3D; HTTPTokenAuth(scheme&#x3D;&#x27;Bearer&#x27;)
`
* Additional Info: `https://flask-httpauth.readthedocs.io/en/latest/`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* Harsh:  Will be Done by KIT team.
### Position: `23:0-24:27`
* Priority: `3`
* Title: `Remove hardcode credentials`
* Category: `Other`
* Description: `Load the configuration from environment.
You need to create a &quot;config.py&quot; module that loads the program globals.
I recommend Pydantic settings.`
* Additional Info: `https://docs.pydantic.dev/2.5/concepts/pydantic_settings/`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* DONE: Need to do.
### Position: `27:0-28:80`
* Priority: `0`
* Title: `Move to config.py`
* Category: `Other`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* DONE: Same as above?
### Position: `32:16-32:16`
* Priority: `0`
* Title: `Do not create a new connection each request`
* Category: `Other`
* Description: `This will generate performance issues, and note that the DB cannot hold to many connections in parallel.
Init should be at the server start, not at each request.`
* Additional Info: `https://python-adv-web-apps.readthedocs.io/en/latest/flask_db1.html#how-to-connect-a-database-to-a-flask-app`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* DONE: Yes
### Position: `34:0-38:56`
* Priority: `0`
* Title: `Move to &quot;authentication.py&quot; module`
* Category: `Other`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
Harsh: Place cred in a authentication.py file.
### Position: `50:0-51:0`
* Priority: `0`
* Title: `Do not start new connection each request`
* Category: `Other`
* Description: `This will generate performance issues, and note that the DB cannot hold to many connections in parallel.
Init should be at the server start, not at each request.`
* Additional Info: `https://python-adv-web-apps.readthedocs.io/en/latest/flask_db1.html#how-to-connect-a-database-to-a-flask-app`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* Harsh: Changed to static class.
### Position: `52:0-56:40`
* Priority: `0`
* Title: `This should be inside the client`
* Category: `Other`
* Description: `You should have a different connection per database, then each method should be using the connections that requires.
For example
 - db_1 &#x3D; init_connection(&quot;http://localhost/database-1&quot;)
 - db_2 &#x3D; init_connection(&quot;http://localhost/database-2&quot;)`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* Harsh: It is not client. It is intance of database.  I mistakengly assinged it to class varaible. 
Harsh: Read this url: https://flask.palletsprojects.com/en/2.3.x/tutorial/database/ and adapt changes to your code.

And, for second part, There is only one database for now.  

### Position: `59:0-60:0`
* Priority: `0`
* Title: `try to simplify`
* Category: `Other`
* Description: `I cannot follow the details of the database.
Maybe you can try to create multiple objects and then merge them together?
Probably wise to move them to a &quot;model&quot; module`
* Additional Info: `https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/models/`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
+ Harsh: Mention schema in schema.py and import it and use it from there, instead of re-init it everytime.
### Position: `62:0-63:0`
* Priority: `0`
* Title: `Server is expected to generate the id`
* Category: `Other`
* Description: `Use a uuid datatype, not a string`
* SHA: `f1bda066f353074a19bc0268ca0abee58a69cff8`
* DONE: Yes
### Position: `67:0-68:0`
* Priority: `0`
* Title: `simplification`
* Category: `Other`
* Description: `Json fields are optional, make it in a way that if the field is pressent, then it means drift is pressent.
Also, &quot;model&quot; seems redundant, would not be better to have it one field up?`
* SHA: `f1bda066f353074a19bc0268ca0abee58a69cff8`
* Harsh: Removed drift. But we need concept and data drift field. As we will be calculating parameters. Say we do not have a drift, we need both of them. If we have drift, we will always have data drift. But we may or may not have concept drift. 
### Position: `130:1-131:0`
* Priority: `0`
* Title: `Do not close the connection`
* Category: `Other`
* Description: `You should simply not execute the query if something fails.
If is the query what fails, pymongo should raise probably the exception? Then you should catch that exception, but probably after that the query was rolledback but the connections is still open (which is correct) `
* Harsh: I removed the close connection. Since connection is always open.
DONE: YES
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
### Position: `138:0-139:0` 
* Priority: `0`
* Title: `Do not close the connection`
* Category: `Other`
* Description: `idem before`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* Harsh: Same as above
DONE: YES
### Position: `149:0-150:0`
* Priority: `0`
* Title: `Do not open new connection`
* Category: `Other`
* Description: `idem before`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* Harsh: Same as above
DONE: YES
## `/backend/requirements.txt`
### Position: `1:0-13:0`
* Priority: `0`
* Title: `fix version ranges `
* Category: `Other`
* Description: `set versions to something for example &quot;Flask ~&#x3D; 3.1.0&quot;
That way, it will not install Flask 3.2.x unless edited.

Otherwise the installation will break when there is a major change in a requirement.`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* Harsh: Change it to approx notation. (~=)
### Position: `2:0-3:0`
* Priority: `0`
* Title: `Split requirements in prodeuction dev and test`
* Category: `Other`
* Description: `Try to min the requirements for production.
For example if you need stramlit only for dev, then move it into another requirements-dev.txt file.`
* SHA: `01b81c1422f3a03d4763c4b5fdba14ea1a8ee1d5`
* DONE: [https://docs.docker.com/build/building/multi-stage/
](https://docs.docker.com/build/building/multi-stage/)
## `/drift_detection/drift_detection.py`
### Position: `154:0-155:0`
* Priority: `0`
* Title: `don&#x27;t use assert in production running code`
* Category: `Other`
* Description: `Assert gets disabled when you run python with “-O” (PYTHONOPTIMIZE), so it is not safe to have it in your deployed code. However, it is correct to use it in tests as PYTHONOPTIMIZE is not designed to be used in testing.`
* Additional Info: `https://docs.python.org/3/reference/simple_stmts.html#the-assert-statement`
* SHA: `ef8bb9ebd872ee91443aef84d1a93a2a3979a549`
* DONE: Yes
## `/docker-compose.yml`
### Position: `5:0-6:0`
* Priority: `0`
* Title: `does not use Dockerfile.mongo`
* Category: `Other`
* Description: `If the file is not used remove. `
* SHA: `ef8bb9ebd872ee91443aef84d1a93a2a3979a549`
* DONE:YES 
### Position: `14:0-15:0`
* Priority: `0`
* Title: `use Dockerfile.api/backend?`
* Category: `Other`
* SHA: `ef8bb9ebd872ee91443aef84d1a93a2a3979a549`
* Harsh: Changed the name to Dockerfile.backend.
* DONE:YES 
### Position: `31:0-32:0`
* Priority: `0`
* Title: `should not be pressent on the service compose`
* Category: `Other`
* Description: `We do not want to deploy a drift_detection container when service is up. This containers should be triggered by the user on demand.`
Harsh: Created a separate docker-compose file for this.
* DONE: YES
* SHA: `ef8bb9ebd872ee91443aef84d1a93a2a3979a549`
### Position: `40:0-41:0`
* Priority: `0`
* Title: `Does not depend on mongodb, only in backend`
* Category: `Other`
* SHA: `ef8bb9ebd872ee91443aef84d1a93a2a3979a549`
* DONE: YES
## `/backend/Swagger_API_doc.yml`
### Position: `12:0-13:0`
* Priority: `0`
* Title: `Non correct url`
* Category: `Other`
* Description: `URL should describe an item in the system.
You post an item: a drift? a result? a status?

&quot;_run&quot; is an action, which one can interprete it will trigger a run.`
* SHA: `f1bda066f353074a19bc0268ca0abee58a69cff8`
DONE: Yes
### Position: `89:0-90:0`
* Priority: `0`
* Title: `non valid URL`
* Category: `Other`
* Description: `save is redundant with POST.
Maybe replace by &quot;POST /job&quot; or &quot;POST /status&quot;`
* SHA: `f1bda066f353074a19bc0268ca0abee58a69cff8`
DONE: Yes
### Position: `122:0-123:0`
* Priority: `0`
* Title: `merge with POST endpoint to edit resources`
* Category: `Other`
* Description: `New endpoint should not be created`
* SHA: `f1bda066f353074a19bc0268ca0abee58a69cff8`
DONE: Yes


https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/models/