import os
import sys
# Get the parent directory of the current script
current_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))

# Add the parent directory to the Python path
sys.path.append(parent_dir)


import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import requests
import json
from datetime import datetime
from datetime import datetime, time
from configuration.settings import Config
import re
st.set_page_config(layout="wide")


def fetch_drift_run_status(username, password, url, start_datetime, end_datetime):
    response = requests.get(url, auth=(username, password), params={"start_date": start_datetime, "end_date": end_datetime})
    if response.status_code == 200:
        data = response.json().get("data", [])
        return data
    else:
        st.warning("No drift run data available for the selected parameters.")
        return []

def extract_number(text):
    # Regular expression to match integers
    match = re.search(r'\d+', text)
    if match:
        return int(match.group())
    else:
        return 0
    
def display_drift_run_status():
    username, password = Config.api_username, Config.api_password
    url = f"{Config.monitoring_url}/get_data_with_multiple_filters"

    start_datetime = st.date_input("Select Start Date", min_value=datetime(2022, 1, 1), max_value=datetime(2025, 12, 31), key="start_date")
    end_datetime = st.date_input("Select End Date", min_value=datetime(2022, 1, 1), max_value=datetime(2025, 12, 31), key="end_date")

    drift_run_status = fetch_drift_run_status(username, password, url, start_datetime, end_datetime)

    if drift_run_status:
        df = pd.DataFrame(drift_run_status)
        st.write("Drift Run Status:")
        st.dataframe(df)
    else:
        st.warning("No drift run status data available.")


def fetch_feature_drift_data(username, password, url, start_date, end_date):
    response = requests.get(
        url,
        auth=(username, password),
        params={"start_date": start_date, "end_date": end_date, "drift_detection": "data_drift"},
    )

    if response.status_code == 200:

        json_data = json.loads(response.text)  
    
        datetime_list = [item['datetime'] for item in json_data["data"]]

        drift_parameter_list = [
            {key: value for feature in item['data_drift']['drift_parameter'] for key, value in feature.items()}
            for index, item in enumerate(json_data["data"])
        ]

        df = pd.DataFrame(drift_parameter_list, index=datetime_list)

        return df

    else:
        st.warning("No drift data available for the selected parameters.")
        
        return None

def fetch_concept_drift_data(username, password, url, start_datetime, end_datetime):
    response = requests.get(url, auth=(username, password), params={"start_date": start_datetime, "end_date": end_datetime, "drift_detection": "concept_drift"})
    if response.status_code == 200:
        json_data = json.loads(response.text)

        data = []

        for item in json_data["data"]:
            data.append({
                "drift_run_id": item["drift_run_id"],
                "Experiment_time": item["datetime"],                
                "index": extract_number(item["concept_drift"]["drift_parameter"][0]["message"]),
                "concept_drift": item["concept_drift"]["drift"]
            })

        df = pd.DataFrame(data)

        return df
    
    else:
        st.warning("No drift data available for the selected parameters.")
        return None

def fetch_no_drift_data(username, password, url_nd, start_datetime, end_datetime):
    response = requests.get(url_nd, auth=(username, password), params={"start_date": start_datetime, "end_date": end_datetime, "drift_detection": "no_drift"})

    if response.status_code == 200:

        json_data = json.loads(response.text)

        data = []
        for item in json_data["data"]:
            
            if "data_drift" in item and "drift_parameter" in item["data_drift"]:                
                if "p_value" in item["data_drift"]["drift_parameter"][0]:
                    print(item)        
                    p_value = item["data_drift"]["drift_parameter"][0]["p_value"]                    
                else:
                    p_value = None

            data.append({
                "drift_run_id": item["drift_run_id"],
                "Experiment_time": item["datetime"],            
                "p_value": p_value,
                "data_drift": item.get("data_drift", {}).get("drift", None)
            })

        df = pd.DataFrame(data)

        return df
    
    else:
        st.warning("No drift data available for the selected parameters.")
        return None


def fetch_jobs_by_status(username, password, url, start_datetime, end_datetime):
    response = requests.get(url, auth=(username, password), params={"start_date": start_datetime, "end_date": end_datetime, "drift_detection": "All"})
    if response.status_code == 200:
        data = response.json()
        
        data = data["data"]
        if data:
            data = [
                {
                    "drift_run_id": job.get("drift_run_id", "123"),
                    "job_status": job.get("job_status", "No status found"),
                    "datetime": job.get("datetime", ""),
                }
                for job in data
            ]

        filtered_data = [job for job in data if job["job_status"].lower() == "started" or job["job_status"].lower() == "failed"]
        return filtered_data
    else:
        st.warning("No job data available for the selected parameters.")
        return None

def display_jobs_by_status():
    st.title(f"Running or Failed jobs")
    
    username, password = Config.api_username, Config.api_password
    url = f"{Config.monitoring_url}/get_data_with_multiple_filters"

    # Select Start Date
    start_date = st.date_input("Select Start Date", min_value=datetime(2022, 1, 1), max_value=datetime(2025, 12, 31))

    # Select Start Time
    start_time = st.time_input("Select Start Time", value=time(0, 0, 0))

    # Combine the selected date and time
    start_datetime = datetime.combine(start_date, start_time)
    start_datetime = start_datetime.strftime("%Y-%m-%dT%H:%M:%S")

    # Select Start Date
    end_date = st.date_input("Select End Date", min_value=datetime(2022, 1, 1), max_value=datetime(2024, 12, 31))

    # Select Start Time
    end_time = st.time_input("Select End Time", value=time(23, 59, 59))

    # Combine the selected date and time
    end_datetime = datetime.combine(end_date, end_time)
    end_datetime = end_datetime.strftime("%Y-%m-%dT%H:%M:%S")
    
    

    jobs = fetch_jobs_by_status(username, password, url, start_datetime, end_datetime)

    if jobs:
        df = pd.DataFrame(jobs)
        df = df.sort_values(by="datetime", ascending=False)
        st.dataframe(df[["job_status", "datetime"]])

def fetch_data(username, password, url, start_date, end_date):
    response = requests.get(
        url,
        auth=(username, password),
        params={"start_date": start_date, "end_date": end_date, "drift_detection": "data_drift"},
    )

    if response.status_code == 200:
        json_data = response.json()
        datetime_list = [item['datetime'] for item in json_data["data"]]
        drift_parameter_list = [
            {key: value for feature in item['data_drift']['drift_parameter'] for key, value in feature.items()}
            for index, item in enumerate(json_data["data"])
        ]
        df = pd.DataFrame(drift_parameter_list, index=datetime_list)
        return df
    else:
        return None

def display_data():
    # Credentials and API URL for feature drift
    username, password = Config.api_username, Config.api_password
    url = f"{Config.monitoring_url}/get_data_with_multiple_filters"

    # Select Start Date
    start_date = st.date_input("Select Start Date", min_value=datetime(2022, 1, 1), max_value=datetime(2025, 12, 31))

    # Select Start Time
    start_time = st.time_input("Select Start Time", value=time(0, 0, 0))

    # Combine the selected date and time
    start_datetime = datetime.combine(start_date, start_time)
    start_datetime = start_datetime.strftime("%Y-%m-%dT%H:%M:%S")

    # Select Start Date
    end_date = st.date_input("Select End Date", min_value=datetime(2022, 1, 1), max_value=datetime(2024, 12, 31))

    # Select Start Time
    end_time = st.time_input("Select End Time", value=time(23, 59, 59))

    # Combine the selected date and time
    end_datetime = datetime.combine(end_date, end_time)
    end_datetime = end_datetime.strftime("%Y-%m-%dT%H:%M:%S")

    # Sidebar for selecting drift type


    # Add a radio button with three options: "Feature Drift", "Concept Drift", and "None Selected"
    selected_option = st.sidebar.radio("Select Drift Type", ["Feature Drift", "Concept Drift", "No Drift Detected"])

    # Set feature_drift and concept_drift based on the selected option
    if selected_option == "Feature Drift":
        feature_drift = True
        concept_drift = False
    elif selected_option == "Concept Drift":
        feature_drift = False
        concept_drift = True
    else:
        # If "None Selected" or no option is selected, set both feature_drift and concept_drift to False
        feature_drift = False
        concept_drift = False


    if feature_drift:
        
        url = f"{Config.monitoring_url}/get_data_with_multiple_filters"

        df = fetch_feature_drift_data(username, password, url, start_datetime, end_datetime)

        if df is not None:
            # Streamlit app for feature drift
            features = list(df.columns)
            selected_features = st.sidebar.multiselect("Select metric", features, default=features)

            if selected_features:
                fig, ax = plt.subplots(figsize=(8, 4))
                for feature in selected_features:
                    color = np.random.rand(3,)
                    ax.scatter(df.index, df[feature], label=feature, color=color)

                ax.set_xlabel("Experiment Date")
                ax.set_ylabel("Feature Values")
                ax.set_title("Feature Drift over Time")
                ax.legend(title="Metrics", loc="upper right")
                 # Rotate x-axis labels at a 45-degree angle
                plt.xticks(rotation=45, ha="right")

                st.pyplot(fig)
            else:
                st.warning("Please select at least one feature.")

    elif concept_drift:
        
        url = f"{Config.monitoring_url}/get_data_with_multiple_filters"
        data = fetch_concept_drift_data(username, password, url, start_datetime, end_datetime)
        
        if data is not None:
            # Create a sample concept_drift plot using plt        
            fig, ax = plt.subplots(figsize=(8, 4))
            plt.scatter(data["Experiment_time"], data["index"], color='#FF0000')
            plt.xlabel("Experiment Date")
            plt.ylabel("Index")
            plt.title("Concept Drift Plot")
            ax.legend(title="Index", loc="upper right")
            # Rotate x-axis labels at a 45-degree angle
            plt.xticks(rotation=45, ha="right")
            st.pyplot(fig)
        
    else:

        url_nd = f"{Config.monitoring_url}/get_data_with_multiple_filters"
        data = fetch_no_drift_data(username, password,  url_nd, start_datetime, end_datetime)
        

        if data is not None:
               fig, ax = plt.subplots(figsize=(8, 4))
               plt.scatter(data["Experiment_time"], data["p_value"], color='#FF0000')
               plt.xlabel("Experiment Date")
               plt.ylabel("P-Value")
               plt.title("No Drift Plot")
               ax.legend(title="P_VALUE", loc="upper right")
               # Rotate x-axis labels at a 45-degree angle
               plt.xticks(rotation=45, ha="right")
               
               st.pyplot(fig)
			


def run_dashboard():
    tabs = ["Job Status", "Completed Jobs"]
    selected_tab = st.sidebar.radio("Select Tab", tabs)

    if selected_tab == "Completed Jobs":
        display_data()
    elif selected_tab == "Job Status":
        display_jobs_by_status()


if __name__ == '__main__':
    run_dashboard()
