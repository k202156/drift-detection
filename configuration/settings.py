import os

class Config:
    mongo_host = "mongodb" 
    mongo_port = 27017    
    mongo_db = "drift-detection-database"
    db_username = "admin"
    db_password = "example"
    drift_run_collection = "drift-run"
    job_status_collection = "job_status"
    swagger_api_doc_url = "./Swagger_API_doc.yml"
    drift_id = "your_generated_drift_id"
    seed = 31
    # monitoring_url = os.environ.get('monitoring_url')
    alpha = 0.01
    epochs = 1
    batch_size = 64
    latent_dim = 5
    file_path = "./train-images-idx3-ubyte.gz"
    upload_url_zenodo = "https://zenodo.org/api/files/4f33cfde-757a-480e-ae34-04fe76396fb6/train-images-idx3-ubyte.gz"
    webhook_url_slack = "https://hooks.slack.com/services/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"
    
    # Specify the headers
    headers_zenodo = {
        "Authorization": f"Bearer {os.environ.get('zenodo_token')}",
        "Content-Type": "application/octet-stream"
    }
    learning_rate = 0.001
    num_samples = 1000
    # add data field folder, where data will be present
    # Update values based on environment

    # Access environment variables
    # api_username = os.environ.get('api_username')
    # api_password = os.environ.get('api_password')

    # for detector
    num_permutations = 200

    # Uncomment to run things locally
    mongo_host = "localhost" # change it to "localhost" when running thing locally
    monitoring_url = "http://0.0.0.0:8000" # change it to "http://flask:8000" when running locally on docker 
    api_username = 'default_username2'
    api_password = 'default_password'
    headers_zenodo = {
        "Authorization": "Bearer QKMbsKo9VD8YagksV89m8NmZECyuBnbP1jRz2SRkTGk4OxkFkfbfQUexlKO3",
        "Content-Type": "application/octet-stream"
    }