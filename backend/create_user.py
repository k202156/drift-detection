import os
import sys


# Add the parent directory to the Python path
current_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))
sys.path.append(parent_dir)


# create_mongodb_user.py
from pymongo import MongoClient
from configuration.settings import Config
# MongoDB connection settings
mongodb_host = Config.mongo_host  # Use the service name defined in Docker Compose
mongodb_port = Config.mongo_port
mongodb_username = Config.db_username
mongodb_password = Config.db_password
database_name = Config.mongo_db
roles_to_create = Config.roles_to_create

# Connect to MongoDB with authentication
client = MongoClient(f"mongodb://{mongodb_username}:{mongodb_password}@{mongodb_host}:{mongodb_port}")

# Check if the database exists
if database_name not in client.list_database_names():
    # Database does not exist, create it
    admin_db = client.admin  # Use the admin database to create the new database
    admin_db.command('create', database_name)
    print(f"Database '{database_name}' created.")

# Access the database
db = client[database_name]

# Check if the user already exists
existing_users = db.command('usersInfo', mongodb_username, showCredentials=True)
if not existing_users['users']:
    # User does not exist, create it
    db.command("createUser", mongodb_username, pwd=mongodb_password, roles=roles_to_create)
    print(f"User '{mongodb_username}' created.")
else:
    print(f"User '{mongodb_username}' already exists.")
