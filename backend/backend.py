
# main.py
import sys
import os
import uuid
import pymongo
import pickle
# Get the parent directory of the current script
current_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.abspath(os.path.join(current_dir, os.pardir))

# Add the parent directory to the Python path
sys.path.append(parent_dir)

import connexion
from pymongo import MongoClient, errors
import jsonschema
from jsonschema import validate
from flask import request, jsonify
import requests
from flask_httpauth import HTTPBasicAuth
import datetime
from bson import ObjectId  
from pathlib import Path
from configuration.settings import Config
from schema import schema
import secrets
import string
import pickle
import base64
import json
from flask import Response
from bson.binary import Binary



class MongoManager:
     __instance = None
     @staticmethod 
     def getInstance():
         if MongoManager.__instance == None:
             MongoManager()
         return MongoManager.__instance
     def __init__(self):
        if MongoManager.__instance == None:
            MONGO_HOST = Config.mongo_host
            MONGO_PORT = Config.mongo_port
            # username = Config.db_username
            # password = Config.db_password

            try:
                # client = MongoClient(
                # host = [ str(Config.mongo_host) + ":" + str(Config.mongo_port) ],
                # serverSelectionTimeoutMS = 3000, # 3 second timeout
                # username = "root",
                # password = "example",
                # )

                MongoManager.__instance = MongoClient(MONGO_HOST, MONGO_PORT)
                
                # uri = f"mongodb://{username}:{password}@{MONGO_HOST}:{MONGO_PORT}/admin"

            except errors.ConnectionFailure:
                MongoManager.__instance = None
                print("MongoDB not initialized")

            print("MongoDB connected")
            

app = connexion.App(__name__, specification_dir="./")
auth = HTTPBasicAuth()
app.add_api(Config.swagger_api_doc_url)

# Initialize the Mongo DB 

def generate_UUID():
    return str(uuid.uuid4())


@auth.verify_password
def verify_password(username, password):
    
    USERNAME = Config.api_username
    PASSWORD = Config.api_password

    return username == USERNAME and password == PASSWORD    

@app.route("/")
def basePage():
    return "<p>Hello, Go to /api/ui to access Drift-Run APIs!</p>"

@app.route('/save_drift_run', methods=['POST'])
@auth.login_required
def save_drift_run_post():
    try:
        mongo_instance = MongoManager.getInstance()

        mongo_db = Config.mongo_db
        drift_run_collection = Config.drift_run_collection

        db = mongo_instance[mongo_db]
        collection = db[drift_run_collection]

        # Document to validate
        drift_run = request.get_json()
        
        drift_run_id = drift_run.get("drift_run_id", None)


        if drift_run_id is None:
            drift_run["drift_run_id"] = generate_UUID()

        # validate(instance=drift_run, schema=schema)        

        # convert to particular format
        drift_run["datetime"] = datetime.datetime.strptime(drift_run["datetime"], "%Y-%m-%dT%H:%M:%S")

        # Insert new document
        collection.insert_one(drift_run)
        message = "Drift run saved successfully"

        return jsonify({"message": message, "drift_run_id": drift_run["drift_run_id"]}), 200

    except jsonschema.exceptions.ValidationError as e:
        if e.path:
            error_field = e.path[0]
            return jsonify({"error": f"Validation error in field '{error_field}': {schema['properties'][error_field]['description']}"}), 400
        else:
            return jsonify({"error": f"Document is not valid. Error: {e.message}"}), 400

    except Exception as e:
        return jsonify({"error": str(e)})


# Save drift run data to DB  
@app.route('/save_drift_run', methods=['PUT'])
@auth.login_required
def save_drift_run_put():
    try:
        mongo_instance = MongoManager.getInstance()

        mongo_db = Config.mongo_db
        drift_run_collection = Config.drift_run_collection

        db = mongo_instance[mongo_db]
        collection = db[drift_run_collection]

        drift_run = json.load(request.files['datas'])             

        if 'document' in request.files:
            pickled_Deserialized_detector = request.files['document'].read()
            drift_run['detector'] = pickled_Deserialized_detector    

        

        # convert to particular format
        drift_run["datetime"] = datetime.datetime.strptime(drift_run["datetime"], "%Y-%m-%dT%H:%M:%S")

        # Check if drift_run_id already exists
        existing_drift_run = collection.find_one({"drift_run_id": drift_run["drift_run_id"]})

        if existing_drift_run:
            # Update existing document
            collection.update_one({"drift_run_id": drift_run["drift_run_id"]}, {"$set": drift_run})
            message = "Drift run updated successfully"
            return jsonify({"message": message, "drift_run_id": drift_run["drift_run_id"]}), 200

    except jsonschema.exceptions.ValidationError as e:
        if e.path:
            error_field = e.path[0]
            return jsonify({"error": f"Validation error in field '{error_field}': {schema['properties'][error_field]['description']}"}), 400
        else:
            return jsonify({"error": f"Document is not valid. Error: {e.message}"}), 400

    except Exception as e:
        return jsonify({"error": str(e)}), 500


@app.route('/get_data_with_drift_id', methods=['GET'])
@auth.login_required
def get_data_with_drift_id():
    try:
        mongo_db = Config.mongo_db
        mongo_instance = MongoManager.getInstance()
        db = mongo_instance[mongo_db]
        drift_run_collection = db[Config.drift_run_collection]

        # Columns to select
        projection = {            
            "drift_run_id": 1,
            "datetime": 1,            
            "concept_drift": 1,
            "data_drift": 1,
            "job_status": 1
        }

        drift_id = request.args.get('drift_id', type=str)

        # if drift_id is provided, filter by drift_id
        if drift_id:
            query = {"drift_run_id": drift_id}
            results = drift_run_collection.find(query, projection)

            count = drift_run_collection.count_documents(query)

            if count > 0:
                # Convert ObjectId to string for serialization
                data_list = [{k: str(v) if isinstance(v, ObjectId) else v for k, v in doc.items()} for doc in results]
                return jsonify({"data": data_list}), 200
            else:
                return jsonify({"message": "no data found for drift_id"}), 204

    except Exception as err:
        return jsonify({"error": str(err)}), 500


@app.route('/get_data_with_multiple_filters', methods=['GET'])
@auth.login_required
def get_data_with_multiple_filters():
    try:
        mongo_db = Config.mongo_db
        mongo_instance = MongoManager.getInstance()
        db = mongo_instance[mongo_db]
        drift_run_collection = db[Config.drift_run_collection]

        # Columns to select
        projection = {            
            "drift_run_id": 1,
            "datetime": 1,            
            "concept_drift": 1,
            "data_drift": 1,
            "job_status": 1,
            "detector":1
        }

        # TODO: Add detector field as a projection in swagger document too.

        start_date = request.args.get('start_date', type=str)
        end_date = request.args.get('end_date', type=str)
        drift_detection = request.args.get('drift_detection', type=str)

        # if parameters are provided, filter by them
        if start_date and end_date and drift_detection:
            projection = {            
            "drift_run_id": 1,
            "datetime": 1,            
            "concept_drift": 1,
            "data_drift": 1,
            "job_status": 1         
        }
            start_date = datetime.datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S")
            end_date = datetime.datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S")

            # Define the initial query without $and
            query = {"datetime": {"$gte": start_date, "$lte": end_date}}

            # Conditions for various drifts
            if drift_detection == "concept_drift":            
                query["concept_drift.drift"] = True
                query["data_drift.drift"] = False
            elif drift_detection == "data_drift":
                query["concept_drift.drift"] = False
                query["data_drift.drift"] = True
            elif drift_detection == "no_drift":
                query["concept_drift.drift"] = False
                query["data_drift.drift"] = False

            results = drift_run_collection.find(query, projection)  
            
            count = drift_run_collection.count_documents(query)

            if count > 0:
                # Convert ObjectId to string for serialization
                data_list = [{k: str(v) if isinstance(v, ObjectId) else v for k, v in doc.items()} for doc in results]
                return jsonify({"data": data_list}), 200
            else:
                return jsonify({"message": "no data found for given filters"}), 204
            
        else:
            detector = drift_run_collection.find({"detector": {"$nin": [None, 'None']}}).sort("datatime", -1).limit(1).next()['detector']
            # pickled_serialized_detector = pickle.dumps(detector)
            pickled_serialized_detector = detector
            # Set the content type header to indicate pickled data
            headers = {'Content-Type': 'application/octet-stream'}

            # Return the serialized pickled data as the response
            
            if detector is not None:
                return Response(pickled_serialized_detector, headers=headers, status=200)
            else:
                return jsonify({"message": "No detector found"}), 404  # Using 404 for "Not Found"

            
            

    except Exception as err:
        print(err)
        return jsonify({"error": str(err)}), 500
    



if __name__ == '__main__':
    
    
    app.run(host="0.0.0.0", port=8000)
    
    # app.run(f"{Path(__file__).stem}:app")
