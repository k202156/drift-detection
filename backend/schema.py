schema = {
            "type": "object",            
            "properties": {     
                "drift_run_id": {"type": "string", format: "uuid", "description": "UUID is required in 'ID' field with length of 12.", "minLength": 12 },
                "job_status" : {"type": "string", "description": "String is required in 'status' field with minimum length of 1.", "minLength": 1},
                "datetime": {"type": "string", "format": "date-time", "description": "String is required in 'datetime' field with minimum length of 1.", "minLength": 1},
                "data_version": {"type":"string", "description": "String is required in 'data_version' field with minimum length of 1."},                
                "model": {"oneOf": [{"type": "string", "format": "binary"},  # Binary data in string format
                    {"type": "null"}  # Null type
                    ], "description": "model is either binary or null"
                },
                "concept_drift": {
                    "type": "object",
                    "properties": {
                        "drift": {"type": "boolean", "description": "Boolean is required in this field.", "not": { "type": "null" }},
                        "drift_parameter": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "patternProperties": {
                                    ".*": {"type": "number", "description": "drift_parameter dict's value should be a number"
                                           }
                                },
                            }
                        },

                    },
                "required": ["drift"]
                },
                "data_drift": {
                    "type": "object",
                    "properties": {
                        "drift": {"type": "boolean", "description": "Boolean is required in this field."},
                        "drift_parameter": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "patternProperties": {
                                    ".*": {"type": "number", "description": "drift_parameter dict's value should be a number"
                                           }

                                },
                            }
                        },                        
                    },
                    "required": ["drift", "drift_parameter"]
                }
            },
            "required": ["drift_run_id", "job_status", "datetime"]
        }