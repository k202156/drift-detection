#!/bin/bash

# clean up containers
bash cleanup_containers.sh

docker-compose -f docker-compose-mongodb-flask-streamlit.yml up -d --build

# Run Docker Compose 
docker-compose -f docker-compose-drift_detection.yml up --build
